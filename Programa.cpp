// Se llaman las librerias a implementar
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sstream>

using namespace std;
// Se llama al archivo que contiene las funciones que crean el programa
#include "Arbol.h"

// Se llama la clase grafo que permitira que se construya.
class Grafo {
  // se definen atributos privados
    private:
        Nodo *nuevoarbol = NULL;    // el nodo nuevo del arbol sera igual a vacio
  // se definen atributos publicos
    public:
        // Constructor de la Clase Graficar.
        Grafo(Nodo *raiz) {
	        this->nuevoarbol = raiz; // el nuevo arbol sera igual a la raiz
        }
        // SE CREA LA FUNCION QUE RECORRE EL ARBOL
        void recorrer_arbol(Nodo *p, ofstream &archivo_id) {
          // Se recorre árbol en preorden y se van agregando los datos al archivo.
        string cadena_id = "\0";
        // si el valor p es distinto de vacio
        if(p!= NULL){
          // se recorrera p por la izquierda y si es distinto de vacio
            if(p->izquierda != NULL){
              // se imprime segun el dato por la izquierda y ademas el factor de equilibrio
                archivo_id << "\"" << p->informacion_dato << "\"->\"" << p->izquierda->informacion_dato << "\" [label=\"" <<p->izquierda->factor_equilibrio << "\"];" << endl;
            }
            // se hace un else que recorre cuando esta vacio por la izquierda
            else{
                cadena_id =  p->informacion_dato + "i";
                archivo_id << "\"" << cadena_id << "\" [shape=point]; " << endl;
                // se agrega la informacion del dato a la cadena
                archivo_id << "\"" << p->informacion_dato << "\"->" << "\"" << cadena_id << "\";" << endl;
            }
            // cuando la p por la derecha no esta vacio
            if(p->derecha != NULL){
                // Se escribe relación entre nodo con la rama derecha.
                archivo_id << "\"" << p->informacion_dato << "\"->\"" << p->derecha->informacion_dato << "\" [label=\"" << p->derecha->factor_equilibrio << "\"];" << endl;
            }
            // de modo contrario si p por derecha es vacio
            else{
                // se apuntara a null
                cadena_id = p->informacion_dato + "d";
                archivo_id << "\"" << cadena_id << "\" [shape=point];" << endl;
                // se agrega la informacion del dato a la cadena
                archivo_id << "\"" << p->informacion_dato << "\"->" << "\"" << cadena_id << "\";" << endl;
            }

            // Se llaman a las funciones que recorren el arbol
            recorrer_arbol(p->izquierda, archivo_id);
            recorrer_arbol(p->derecha, archivo_id);
        }
    }
    // se crea la funcion que genera el grafo
    void crear_grafo() {
      ofstream archivo_id;
      // Se abre el archivo en el que se guardará la información
      archivo_id.open("grafo.txt");
      // Se recorre el archivo y tambien se dan los atributos caracteristicos del grafo como su color
      archivo_id << "digraph G {" << endl;
      archivo_id << "node [style=filled fillcolor=yellow];" << endl;
      archivo_id << "nullraiz [shape=point];" << endl;
      // se recorre el arbol con la informacion y los factores de equilibrio
      archivo_id << "nullraiz->\"" << this->nuevoarbol->informacion_dato << "\" [label=" << this->nuevoarbol->factor_equilibrio<< "];" << endl;
      // recorrera el arbol y lo añadira con los archivos
      recorrer_arbol(this->nuevoarbol, archivo_id);
      archivo_id << "}" << endl;
      // el archivo se cierra
      archivo_id.close();
      // se genera el txt del grafo que luego se convierte a imagen
      system("dot -Tpng -ografo.png grafo.txt &");
      system("eog grafo.png &");
    }
  };

// Se crea el menu de opciones para que el usuario lo pueda ver
int opciones_menu(){
    int opcion;
    // Se imprimen las opciones
    cout << "\n MENU OPCIONES ARBOL \n" << endl;
	  cout << "OPCION 1 - INGRESAR TODOS LOS ID DE UN ARCHIVO" << endl;      //OPCION QUE INGRESA TOODS LOS ID DEL ARCHIVO (170.000)
    cout << "OPCION 2 - INGRESAR UN GRUPO DE ID DE UN ARCHIVOS" << endl;      //OPCION QUE INGRESA UN GRUPO DE ID (200)
    cout << "OPCION 3 - INGRESAR OTROS ID" << endl;                         //OPCION QUE INGRESA DISTINTOS ID
	  cout << "OPCION 4 - ELIMINAR" << endl;                                 //OPCION QUE PERMITE ELIMINAR LOS ID
    cout << "OPCION 5 - MODIFICAR" << endl;                               //OPCION QUE MODIFICA LOS ID
    cout << "OPCION 6 - VER GRAFO " << endl;                              //OPCION QUE PERMITE VER EL GRAFO
	  cout << "OPCION 0 - SALIR DEL PROGRAMA" << endl;                     //OPCION QUE PERMITE SALIR DEL PROGRAMA

	// Se imprime para que el usuario ingrese su opcion y se captura el valor
	cout << "Ingrese su opcion:";
	cin >> opcion;
	return opcion;
}

// SE GENERA EL MAIN DONDE SE LLAMAN LAS FUNCIONES
int main(void){
  // SE DEFINEN LAS VARIABLES Y SUS ATRIBUTOS
	int opcion;
  // LOS DATOS A AÑADIR SON LOS SIGUIENTES Y TIENEN CARACTERISTICAS DE STRING
	string dato_id2;
	string dato_nuevo;
  Nodo *raiz = NULL;    // EL NODO RAIZ SERA NULL
  bool resultado;       // EL RESULTADO SERA BOOLEANO
	Arbol arbol;         // EL ARBOL ESTARA DEFINIDO
	int altura_arbol;

	opcion=1;			//Para iniciar el ciclo While
	while (opcion != 0){		// Comienza el ciclo while de las opciones
		opcion = opciones_menu(); // Se llaman las opciones
		switch (opcion){          // Se genera el ciclo switch case
		// En el caso 1 se leen todos los datos
    case 1:
      cout << "LEYENDO DATOS ARCHIVOS" << endl;
      cout << "CUIDADO LA OPCION 1 CARGARA LOS 170.000 DATOS ES PROBABLE QUE LA IMAGEN RALENTIZE SU PC" << endl;
      // se llama a la funcion que lee los datos
      arbol.leer_datos(&raiz, &altura_arbol);
      break;
      opciones_menu();
    // En el caso 2 se lee solo un grupo de id de los archivos
    case 2:
      cout << "LEYENDO 160 ID DEL ARCHIVO " << endl;
      // se llama la funcion que lee esos archivos
      arbol.leer_datos2(&raiz, &altura_arbol);
      break;
      opciones_menu();
    // En el caso 3 se pueden ingresar otros valores
		case 3:
			cout << " INGRESANDO OTROS ID" << endl;
      cout << "Ingrese el ID a agregar" << endl;
			cin >> dato_id2;
      // se llama a la funcion que ingresa balanceadamente los datos
			arbol.InsercionBalanceado(&raiz, &altura_arbol, dato_id2);
			break;
			opciones_menu();
    // En el caso 4 se eliminan los datos
		case 4:
			cout << " ELIMINANDO DATOS" << endl;
      cout << " RECUERDE CARACTERES EN MAYUSCULA" << endl;
      // Se pregunta el dato a eliminar
			cout << "Ingrese el numero a eliminar" << endl;
			cin >> dato_id2;
      // Se llama a la funcion que elimina
			arbol.EliminarBalanceado(&raiz, &altura_arbol, dato_id2);
			break;
      opciones_menu();

		// En el caso 5 vemos la funcion que modifica los valores
    case 5:
      cout << " MODIFICANDO VALORES " << endl;		// Se imprime que los datos son modificados
      cout << " RECUERDE CARACTERES EN MAYUSCULA" << endl;
    	cout << " Ingrese dato Actual: " << endl;		// Se pide el valor actual a modificar
    	cin >> dato_id2;
    	cout << " Ingrese dato Nuevo: " << endl;		// Se pide el nuevo valor que reemplazara al modificar
    	cin >> dato_nuevo;
      // Se llama a la funcion que modifica el nodo
    	arbol.modificacion_nodo(&raiz, &altura_arbol, dato_id2, dato_nuevo);
      break;
      opcion = opciones_menu();
    // En el caso 6 se muestran los contenidos del grafo
	  case 6:
      cout << " MOSTRANDO CONTENIDO DE LOS GRAFOS " << endl;
      // Se mostrara la representación grafica del árbol
      Grafo *grafo = new Grafo(raiz);
      grafo->crear_grafo();
      break;
      opciones_menu();
		}
	}
	return 0;
}