#ifndef ARBOL_H
#define ARBOL_H
// SE LLAMAN A LAS LIBRERIAS PARA TRABAJAR CON STRING Y LOS ARCHIVOS
#include <iostream>
#include <string.h>
#include <fstream>
using namespace std;
// Se llama al archivo que tiene la definicion del nodo
#include "Nodo.h"

// SE CREA LA CLASE ARBOL
class Arbol{
	// SE ESTABLECEN ATRIBUTOS PRIVADOS
    private:
      // SE DEFINE EL NODO NUEVO NODO QUE ESTA VACIO
      Nodo *nuevonodo = NULL;

	// SE ESTABLECEN LOS ATRIBUTOS PUBLICOS
    public:
        Nodo *raiz = NULL;                          // SE DEFINE EL NODO NUEVO NODO QUE ESTA VACIO
        Arbol();                                    // SE DEFINE EL CONSTRUCTOR ARBOL
        Nodo* agregar_nodo(string dato_id);					// SE DEFINE AL NODO QUE AGREAGA A LOS DEMAS
        void InsercionBalanceado(Nodo **raiz, int *altura_arbol, string dato_id );	// SE CREA FUNCION QUE AÑADE LOS ID AL NODO
        void ReestructuraDer(Nodo **raiz,int *altura_arbol);						// FUNCION QUE REESTRUCTURA LA DERECHA
        void ReestructuraIzq(Nodo **raiz, int *altura_arbol);           // FUNCION QUE REESTRUCTURA LA IZQUIERDA
        void Borra(Nodo **aux1, Nodo **otro1, int *altura_arbol);					// FUNCION QUE ELIMINA DATOS (BORRA)
        void EliminarBalanceado(Nodo **raiz, int *altura_arbol, string dato_id);	// FUNCION QUE ELIMINA EL NODO DEL ARCHIVO
        bool busqueda_nodo(Nodo *nodo0, string dato_id);    // FUNCION QUE BUSCA EL ID EN EL NODO PARA MODIFICARLO
        bool modificacion_nodo(Nodo **raiz, int *altura_arbol, string dato_id, string dato_nuevo); // FUNCION QUE MODIFICA LOS NODOS Y LOS ID
        void leer_datos(Nodo **raiz, int *altura_arbol); // FUNCION QUE LEE TODOS LOS ARCHIVOS QUE CONTIENEN LOS ID
        void leer_datos2(Nodo **raiz, int *altura_arbol); // FUNCION QUE LEE EN MENOR CANTIDAD LOS ARCHIVOS
};
#endif
