// Se declaran las librerias a utilizar los string dentro del nodo
#include <iostream>
#include <string.h>
// Se define la estructura del nodo
#ifndef NODO_H
#define NODO_H
// Se define la estructura del nodo
typedef struct _Nodo {
    string informacion_dato;          // Se define de tipo string la informacion del dato
    int factor_equilibrio = 0;        // Se define el factor de equilibrio
    struct _Nodo *izquierda;          // Se define la estructura por la izquierda
    struct _Nodo *derecha;            // Se define la estructura por la derecha

}Nodo;
#endif
