# Guia 5 --- ARBOLES BALANCEADOS, LECTURA DE DATOS DE ID DE PROTEINAS DESDE UN ARCHIVO Y GRAFO ---

# Comenzando --- EXPLICACION PROBLEMATICA----
Segun unos archivos de texto entregado por el profesor que se encuentran los documentos en Educandus tienen ciertos identificadores de las estructuras de proteinas en el Protein Databank, tambien se pueden entender los algoritmos del libro base del curso en el cual se puede acceder para la construccion del codigo que resuelva la problematica de la guia. Se debe reescribir el programa en el lenguaje C a C++ en donde se debe crear un arbol balanceado que contenga la informacion de los ID de las estructuras de las proteinas, se tiene que leer la informacion previa de los archivos. Tambien se tiene que permitir insertar, eliminar, modificar el ID. Posteriormente a la problematica se debe generar un grafo que corresponsa a la estructura de mostrar la relacion entre los nodos y los valores. 
Este tipo de dato debe mantener los elementos ordenados al ser ingresado, tiene tambien que el programa un menu basico para que finalice el programa y se pueda tener una interaccion con el usuario luego de ingresar una opcion, se tiene que implementar clases para el desarrollo y ciertas validaciones para que el programa opere de manera eficiente. 

## ---- COMO SE DESARROLLO ----
Para poder encontrar una solucion a la problematica propuesta se realizo una modificacion y reutilizacion al codigo entregado por el profesor el cual estaba en lenguaje C, al lenguaje C++ tambien orientandolo a objetos y permitiendo el uso de arboles balanceados. Se utilizaron 5 archivos los cuales permiten el correcto funcionamiento del programa los cuales se detallan a continuacion y con la explicacion de cada una de las funciones que los componen. 
Los archivos con los cuales se construyo la solucion al programas son: 

- _Makefile_:  Permite que se pueda compilar el codigo mediante la terminal y desarrollar las pruebas de ejecucion en donde se indican los posibles errores del codigo y a la vez indica si la compilacion se ha desarrollado de manera exitosa. Es un requerimiento dentro de los programas para poder ver su funcionalidad. 

- _Nodo.h_: Este archivo contiene la estructura basica del nodo la cual permite que pueda ser llamado en los distintos archivos de esta manera se puede desarrollar de manera global y contiene todas las estructuras necesarias las cuales pueden ser los nodos izquierda y derecha que permiten que se recorran los extremos del arbol y ademas se agregan el tipo de los datos y las variables que tambien son parte del nodo como por ejemplo el factor de equilibrio (int) y a su vez la informacion de dato (string debido que son letras y numeros)

- _Arbol.h_: Este es el archivo cabecera del archivo Arbol.cpp, contiene la estructura del nodo para que el código pueda funcionar y se genere esta estructura se genera otra clase llama Árbol, esta contiene atributos públicos y privados los cuales son requeridos para el funcionamiento y detalle del programa como un nuevonodo y la raiz del arbol. En los atributos públicos se define el constructor, también se llaman a ciertos nodos que permiten agregar otros. También se generan las funciones principales las cuales permiten agregar los valores del ID al nodo, poder eliminar los ID , llevar a cabo la búsqueda y a su vez luego de buscar poder modificar los nodos si es que se encuentran los numeros (estas dos últimas funciones tienen las características de ser booleanas, lo que significa que retornan un true o un false).  Tambien estan las funcion Reestructura Izq y Derecha las cuales permiten luego de las modificaciones que el nodo sea reestructurado y el arbol pueda ser modificado segun la rama que se modifica. Finalmente cada detalle de la función y explicación de esta será indicada en su archivo, debido a que este es solo el archivo de cabecera. Ademas se agrego dos funciones las cuales permite poder leer la informacion del archivo.txt que contiene la informacion de las proteinas en este caso se generaron dos archivos, el primero contendra los 170.000 ID y posteriormente el segundo archivo contiene un aproximado de 160 datos para que se puedan visualizar en el grafo porque los 170.000 no permiten generar una imagen clara porque son muchos datos y no hay memoria suficiente. 

- _Arbol.cpp_:  En este archivo se llama al archivo cabecera el cual contiene las indicaciones de como deben funcionar las funciones, se llama al constructor de las funciones que en este caso es Arbol. Se crea un nodo nuevo el cual permite que se agreguen a una clase arbol los nodos a partir de la izquierda y la derecha(nodos anteriormente creados). 
Luego se crea la funciones que permiten el funcionamiento del programa, las cuales son:

    - _agregar_nodo():_ Esta funcion permite que se puedan agregar los nodos al arbol a su vez tambien se definen los tipos y las caracteristicas de los nuevos nodos por la izquierda y derecha, a su vez se define un nuevo nodo, tambien se define la caracteristica de que los datos que se añadan al arbol sean de caracter string.

    - _InsercionBalanceado():_ Para poder insertar elementos al nodo se requiere la altura del arbol, el nodo raiz y a su vez el factor de equilibrio para poder ir organizando los datos y que estos tengan un sentido antes y despues de las rotaciones. Para poder hacer las rotaciones se tienen que ver los casos cuando se tiene la misma altura del arbol en primera instancia se ve ademas de que los valores sean menores a la informacion se ira añadiendo por la izquierda del nodo los valores, tambien se tiene que ir analizando cuando un nodo es menor o mayor a otro. Para esta funcion se requieren de nodo auxiliares para ir estableciendo las comparativas entre los datos que ingresa el usuario y las posiciones del nodo por la derecha y por la izquierda. Tambien cuando el factor de equilibrio apunta al nodo y la informacion se llevaran a cabo 2 casos donde tendran valores -1, 1, 0 en donde dependiendo del valor del factor de equilibrio sera igualado al nodo. Tambien se hace las comparativas correspondientes con los otros dos auxiliares, primero con el nodo1 cuando tiene un factor de equilibrio menor o igual a 0 de se le entregan nuevos valores a los demas nodos y a los factores de equilibrio cuando recorran la derecha e izquierda del nodo. 
Cuando el nodo dato del usario es mayor al nodo que contien la informacion se añaden por la derecha los valores, para esto se tiene que hacer una comparativa con la altura y el factor de equilibrio si este tiene valores -1, 0, 1 se tienen que ir reemplazando los valores con los facotres de equilibrio. Tambien se hace las comparativas correspondientes con los otros dos auxiliares, primero con el nodo1 cuando tiene un factor de equilibrio mayor o igual a 0 de se le entregan nuevos valores a los demas nodos y a los factores de equilibrio cuando recorran la derecha e izquierda del nodo. 
Ademas se hara una validacion si es que el nodo ya se agrego y no cumple las condiciones de añadirse porque esta en el arbol se indica este comentario al usuario.
Posterior a realizar todas las acciones de ver los nodos y añadir, se creara un nuevo nodo en donde se añaden los valores, tambien se definen las condiciones que tendran los nodos por derecha e izquierda y la igualacion con la variable definida string y el dato que ingrese el usuario. 
_La finalidad de esta funcion es agregar de manera balanceada por la derecha e izquierda y poder ir viendo los nodos y los valores que tienen en las ramas, para recorrer el arbol se llevan a cabo unas rotaciones DI, DD, ID, II (Las rotaciones son escenciales para ver los desbalances y tratarlos) para agregar los datos luego de buscar la posicion y que los datos que se agregen tengan una correlacion con el factor de equilibrio y la altura_
   
    - ReestructuraDer(): Esta funcion permite reestructurar el nodo por la derecha se hacen las comparativas cuando la altura es verdadera con los factores de equilibrio del nodo 0 cuando tengan los casos y valores -1,0,1.  Primero se hace una rotacion de  derecha a derecha y posteriormente de derecha a izquierda comparando los tres nodos en cuestion y estableciendo los valores de los factores de equilibrio de cada uno de los nodos dependiendo el caso y la localizacion. Finalmente la raiz contendra el nodo con las condiciones que se realizaron. 

    - ReestructuraIzq(): Esta funcion permite reestructurar el nodo por la izquierda se hacen las comparativas cuando la altura es verdadera con los factores de equilibrio del nodo 0 cuando tengan los casos y valores -1,0,1.  Primero se hace una rotacion de los nodos de izquierda a izquierda y luego se rota desde la izquierda a derecha. Se comparando los tres nodos en cuestion y estableciendo los valores de los factores de equilibrio de cada uno de los nodos dependiendo el caso y la localizacion. Finalmente la raiz contendra el nodo con las condiciones que se realizaron. 
_La finalidad de la reestrcuturacion es un proceso en donde se tiene que ir recorriendo el arbol y luego se va viendo el lugar en donde se tiene que insertar o eliminar segun sea el caso, se tiene que tambien depender del factor de equilibrio y los nodos de este. se tiene que terminar cuando llefa a la raiz del arbol. Para hacer la reestrcuturacion se tienen que ir rotando los nodos del factor de equilibrio este proceso puede ser simple por las ramas derecha e izquierda (II, DD) o por las ramas compuestas DI, ID, Las rotaciones son escenciales para ver los desbalances y tratarlos_

    - _Borra()_: Esta funcion permite poder borrar el nodo del arbol para esto se tiene que buscar la posicion en donde esta, de esta manera tambien se va recorriendo por meido de auxiliares para la izquierda y la derecha tambien a su vez va reestructurandolo por ejemplo cuando borra a la derecha posteriormente ocupa la ReestructuraIzq. En lineas generales hace comparativas si el nodo por la derecha es diferente a vacio y borra el nodo. De modo contrario solo recorrera por la izquierda y viendo la informacion de los datos.

    - _EliminarBalanceado():_ Esta funcion permite que se elimine de manera balanceada los datos que el usuario ingresa de esta manera se recorren los nodos del arbol por la derecha y por la izquierda, siempre cuando las condiciones se cumplen el dato es eliminado y posteriormente reestrcuturado por el lado que le corresponde, tambien se hacen comparaciones de los nodos para poder buscar la que el nodo que se busca se encuentre y no se elimine un nodo que no exista. Se requiere que la eliminacion sea balanceada para que los datos no presenten errores. tambien se llama a la funcion borrar cuando necesita la eliminacion total del nodo. los valores auxiliares luego son liberados. Si el usuario ingresa un numero desconocido y que no esta en el nodo este mostrara un mensaje que no esta en el arbol.

    - busqueda_nodo(): Esta funcion de tipo booleano permite poder retornar el puntero con el valor ingresado, cuando el dato a buscar se   ingrese por el usuario, se busca el nodo y se recorre el arbol por medio del nodo de la raiz, cuando el valor es igual al dato este estara a la derecha, de modo contrario estara a la izquierda y se retornara si lo encontro al usuario. Esta funcion permite poder validar que los numeros no sean repetidos al ser ingresados. En lineas generales busca si el nodo existe y para esto retornara un true o false.

    - modificacion_nodo(): Esta funcion es de tipo booleana y permite establecer las comparativas de las funciones por medio de la busqueda, esta funcion permite comparar si el dato existe dentro del arbol, si este no existe no se puede completar la operacion y sale un mensaje de advertencia, tambien a su vez hace otra busqueda su no se puede encontrar el nodo en el arbol. Finalmente luego de realizar las busquedas si encuentra al dato que el usuario quiere modificar se puede proceder a eliminar el nodo anterior y añadir al nodo el nuevo dato del arbol.

    - leer_datos(): Esta funcion se referencio de unas busquedas de internet en donde por medio de la libreria fstream se pudo abrir el archivo de texto y finalmente recopilar los datos de este para poder insertarlos en el arbol por medio de la funcion _InsercionBalanceado_, a su vez esta funcion tiene que tener la altura del arbol y el nodo de la raiz. El archivo que se llama _pdblist.txt_ contiene la informaicon de los 170.000 ID de las proteinas, este archivo fue creado mediante el comando:
        - _cat > (rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b.txt)_
        En donde se pudo recopilar toda la informacion de los ID en ese solo archivo.
    
    - leer_datos2(): Esta funcion se referencio de unas busquedas de internet en donde por medio de la libreria fstream se pudo abrir el archivo de texto y finalmente recopilar los datos de este para poder insertarlos en el arbol por medio de la funcion _InsercionBalanceado_, a su vez esta funcion tiene que tener la altura del arbol y el nodo de la raiz. Esta funcion permite leer el archivo _pdblist2.txt_ en donde se selecciono una cantidad especifica de ID (160) definidos por el autor del codigo para que de esta manera el usuario puediese visualizar el grafo con una menor cantidad de datos. 

-_Programa.cpp_: En este archivo se llevan a cabo la funcionalidad del programa la cual puede ser probada y ademas se lleva a cabo la interaccion con el usuario. 
Se incluyen las librerías a implementar y también se llama al archivo que contiene las funciones para que se lleve a cabo el programa(cabecera). En este archivo también se genera la _clase Grafo_ esta tiene sus atributos públicos y privados los cuales permiten que se construya un nodo para poder mostrar el funcionamiento de este en el grafo. Dentro de esta misma clase se crea una función que permite crear el grafo llamando los archivos para que sean abiertos, para que se generen y para también indicar el estilo de este al mostrarlo, luego se obtiene un archivo de texto la cual se transforma mediante system al formato imagen. Posterior a esta función se crea un recorrido la cual permite recorrer por la izquierda y la derecha el nodo, cada vez que se agregue el valor y la dirección este será agregado al nodo mostrando así los nodos padre hijos y hermanos. Esta función es similar y se basó en lo entregado por el profesor Alejandro Valdes en donde se tienen que ir agregando comillas simples antes de las figuras del grafo, se tiene que recorrer a su vez con la informacion_dato y con el factor_equilibrio.
Por medio de las funciones _recorrer_arbol_ se puede recorrer en preorden la estructura del arbol y pasarlo a un nodo recorrido para luego poder construir el grafo, se establecen a su vez las condiciones por la izquierda y derecha de los nodos con los puntos y nuevos nodos para poder mostrar por condiciones especificas los valores que ingrese el usario tambien dentro de esta funcion se reocrre con la informacion de los datos y a su vez con el factor de equilibrio con el fin de poder evidenciar el Arbol Balanceado y los coeficientes de este. Ademas por la funcion _crear_grafo_ se permite poder abrir el archivo y generar el diagrama tambien permite que se puedan tratar los temas esteticos del diagrama como por ejemplo el color y el estilo. Luego de crear los grafos se crea un archivo txt el luego se convierte a imagen y se muestra al usuario por medio de la opcion que este digite.  El objetivo de esta clase es poder generar el grafo y a su vez recorrer el funcionamiento. 

Posteriormente esta la funcion _opciones_menu()_: Se encarga de contener la acciones generales que el usuario ejecute en el código, cada opción se guardará y posteriormente en el main se ejecuta la acción. Esta opcion permite al usuario poder añadir y ejecutar la accion que requiera con los ID como por ejemplo ir modificando y eliminando y añadiendo nuevos ID. Tambien el usuario podra ir ingresando los ID de un archivo con 170.000 ID como primera opcion y tambien puede ir ingresando un grupo de ID del archivo que tiene la informacion. Los grafos se pueden visualizar mejor con la segunda opcion debido a que el primer archivo de texto contiene una cantidad superior a su vez no se puede generar una imagen.  

Ademas tambien esta la funcion _int_main()_: donde se lleva a cabo la funcionalidad del programa y el árbol, se llaman a las opciones del menú y según la opción que el usuario ingrese se llamará a las funciones en el archivo _arbol.cpp_. El primer caso son añadir los archivos segun lo que desea el usuario en este caso puede ser agregar todos los ID del archivo o bien el segundo caso seria agregar una menor porcion de los ID (160) posteriormente a este ingreso o antes se pueden añadir mas valores de ID los cuales. su vez se añaden al arbol a su vez permiten ciertas validaciones para que los datos que se ingresen esten presentes en la lista y si lo estan no se vuelvan a ingresar tambien se lleven a cabo las rotaciones de ID, DI, II, DD. Ademas se pueden eliminar de forma balanceada los valores que se ingresar por el usuario. Tambien se permite hacer modificaciones con ellos cuando el usuario ingresa el valor actual y el que se desea cambiar se llaman a las funciones creadas con anterioridad para poder verficiar que estas ocurran de manera adecuada y que el numero que se desea cambiar exista en el arbol. Posteriormente a todas estas opciones el usuario puede visualizar los grafos con los arboles de manera balanceada con la segunda opcion de los ID (que pueden sufrir las modificaciones descritas anteriormente) puesto que con la primera opcion no se pueden cargar en su totalidad porque son muchos y no se puede generar una imagen. Se eligio 160 debido a que de esta manera se puede visualizar el arbol de buena manera y ver los atributos y como se balancea.

_Finalemente podemos decir que el funcionamiento del programa es correcto y logra resolver la problematica propuesta de la quinta guia, ademas se realizan de manera correcta las funciones eliminar, insertar y modificar datos tanto nuevos como de un archivo. Tambien permite poder leer los datos de un archivo completos (170.000) como de un archivo de menor tamaño (160), permite a su vez visualizar mediante un grafo los datos que se añadan y los del archivo de menor tamaño, porque los de mayor tamaño no pueden generar una imagen por lo que el programa no puede generarla, pero con datos menores a 1000 se puede lograr, se eligio 160 porque se ve como esta balanceada y como estan los valores de los factores de equilibrio. El grafo presenta una advertencia la cual no impide el funcionamiento del programa y permite seguir ingresando las opciones al gusto del usuario, finalizando puede cerrar el programa por medio de la opcion 0._

# Prerequisitos
- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)

# Instalacion
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:

- _lsb_release -a (versión de Ubuntu)_

En donde: Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando. Para descargar un editor de texto como vim se puede descargar de la siguiente manera:

- _sudo apt install vim _

En donde: Por medio de este editor de texto se puede construir el codigo. En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.
Tambien si se desea ocupar un editor de texto diferente a vim este se puede instalar por la terminal o por el centro de software en donde se escribe el editor que se desea conseguir y luego se ejecuta la descarga, de esta manera se pudo descargar el editor que se ocupa para la realizacion de esta guia el cual es atom, se eligio a la vez este editor debido a que permite una interfaz mejorada con respecto a la compilacion y a su vez permite que se revisen y compilen los archivos de la terminal disminuyendo posibles errores.

# Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa

### INSTALACION MAKEFILE
Para poder llevar a cabo el funcionamiento del programa se requiere la instalacion del Make para esto primero hay que revisar que este en la computadora, para corroborar se debe ejecutar este comando:
- _make_
De esta forma se puede ver si esta instalado para luego llevar a cabo el desarrollo del programa, si este no esta instalado, se tiene que insertar el siguiente comando en la terminal de la carpeta del programa.
- _sudo apt install make_
De esta manera se procede a la creacion del make para que luego este se lleve a cabo y se pueda observar la construccion del archivo.
Luego de instalar el Make, para poder ejecutar las pruebas se tiene que en la terminal entrar en la carpeta donde se encuentran los archivos mediante el comando cd con el nombre de dicha carpeta, luego ver si estan los archivos correctos o vizualizar lo que contiene la carpeta se debe ingresar el comando ls, finalmente se debe escribir make en la terminal.
Si ya se ha ejecutado con anterioridad un make, se tiene que escribir un make clean para borrar los archivos probados con anterioridad. Luego de ejecutar en la terminal el comando:
- _make_
Debe mostrar los nombres de los archivos y el orden de estos con g++ y el nombre del archivo, aqui se puede visualizar si el programa no tiene problematicas. Si hay una problematica sale un mensaje y no salen todos los archivos con g++. Si se desarrollo completamente luego se puede ingresar el siguiente comando:
- _ ./ con el nombre del programa el cual contiene el main._
De esta manera se observa el funcionamiento del programa y de los demas archivos.

### INSTALACION GRAPHVIZ
Este paquete es un requisito para poder generar y visulizar los grafos creados durante el programa.

#### _GENERACION EN LINEA_
La generacion en linea de los grafos se puede desarrollar de manera online mediante el siguiente sitio:
- http://graphs.grevian.org/graph
En donde se tiene que ingresar la definicion del DOT en el recuadro que se puede visualizar en la pagina y de esta manera se puede visulaizar el grafico resultante para ser descargado.

#### GENERACION DE MANERA LOCAL
Para instalarlo de manera local en Ubuntu tanto como en Debian el usuario debe ejecutar el siguiente root:
- _apt-get install graphviz (Debian)_
- _sudo apt-get install graphviz (Ubuntu)_
De esta manera se podra instalar el paquete y comenzar a desarrollar los grafos.
Posteriormente para poder generr la imagen a partir del archivo fuente se tiene que ingresar en la terminal:
- _dot -Tpng -ografo.png datos.txt_
En donde se generara el archivo con la imagen grafo.png ( grafo simula al nombre del archivo)
Luego para poder visulaizar la imagen se tiene que ejecutar el siguiente comando:
- _eog grafo.png_
De esta forma a partir de un archivo de fuente se puede obtener el archivo que visualice la imagen posterior a la generacion de esta.

# Construido con
- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Atom: Editor de código.
- GRAPHVIZ: Herramienta que permite la visualizacion de los grafos.

# Versiones
## Versiones de herramientas:
- Ubuntu 20.04 LTS
- Atom 1.52.0
- GRAPHVIZ 2.40

Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/guia5

# Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

# Expresiones de gratitud
- A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=731003 y ademas a Mauricio ayudante del modulo a poder solucionar dudas y problematicas.

- A las Lecturas de internet y videos que me ayudaron a plantear una solucion posible:
    - https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiiobv2w5vtAhU2DrkGHfahAYoQFjABegQIBxAC&url=https%3A%2F%2Fwww.lawebdelprogramador.com%2Fforos%2FDev-C%2F1469895-Anadir-datos-a-fichero-de-texto-C.html&usg=AOvVaw1zpiQHbV97rH1iWlmfZ2jY

    - https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiiobv2w5vtAhU2DrkGHfahAYoQFjAAegQICBAC&url=https%3A%2F%2Fes.stackoverflow.com%2Fquestions%2F312168%2Fcomo-insertar-datos-en-arrays-desde-un-archivo-txt-c&usg=AOvVaw1vOCxpLI5XAJ7Z1Jr9X_q7

    - https://estructuraddatosblog.wordpress.com/2016/03/01/arboles-avl/
