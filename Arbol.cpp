//se definen las librerias a implementar
#include <iostream>
#include <string.h>
#include <fstream>

// Definición de la clase y se llaman las funciones de cabecera
#include "Arbol.h"
using namespace std;
// SE LLAMA AL CONSTRUCTOR DE LA CLASE
Arbol::Arbol(){
}

// NODO QUE SE ENCARGA DE AGREGARLO A LA CLASE ARBOL
Nodo* Arbol::agregar_nodo(string dato_id){
  Nodo *nuevonodo;				//Crea un puntero del tipo NODO
  nuevonodo = new Nodo;			//Se define una estructura del tipo nodo
  nuevonodo->informacion_dato = dato_id;		//Se incorpora el valor
  nuevonodo->izquierda = NULL;	//Se asigna NULL al puntero IZQUIERDA
  nuevonodo->derecha = NULL;		//Se asigna NULL al puntero DERECHA
  return nuevonodo;				//Se devuelve el puntero al nuevo nodo Creado
}

//Se crea la funcion que inserta balanceadamente los datos del id
void Arbol:: InsercionBalanceado(Nodo **raiz, int *altura_arbol, string dato_id) {
  // ESTOS NODOS AYUDAN A RECORRER LA RAIZ AL HACER LAS ROTACIONES
  Nodo *nodo0 = NULL;
  Nodo *nodo1 = NULL;
  Nodo *nodo2 = NULL;
  // se hace una igualacion del nodo 0 y raiz
  nodo0 = *raiz;
  // cuando el nodo0 es diferentede vacio
  if (nodo0 != NULL) {
    if(dato_id < nodo0->informacion_dato){        // el dato es menor a la informacion
      InsercionBalanceado(&(nodo0->izquierda), altura_arbol, dato_id); // se inserta balanceadamente a la izquierda
      // si el la altura es verdadera
      if(*altura_arbol == true) {
        switch (nodo0->factor_equilibrio) {  // se hara un ciclo switch case para ver el factor de equilibrio
          // si es 1, el factor es igual a 0 y la altura es falsa
          case 1:
            nodo0->factor_equilibrio = 0;
            *altura_arbol = false;
            break;
        // si el caso es 0, el factor de equilibrio sera igual a -1
          case 0:
            nodo0->factor_equilibrio = -1;
            break;
      // si el caso es igual a -1
          case -1:
            ///se reestructura el arbol por la izquierda
            nodo1 = nodo0->izquierda;
            // Se hace la rotacion II, el factor de equilibrio es menor e igual a 0
            if(nodo1->factor_equilibrio <= 0) {
              // se ve la derecha e izquierda del nodo
              nodo0->izquierda = nodo1->derecha;
              nodo1->derecha = nodo0;
              // el factor de equilibrio es 0
              nodo0->factor_equilibrio = 0;
              // se igualan los nodos
              nodo0 = nodo1;

            } else {
              // De modo contrario se hace una rotacion ID
              // se ve el nodo por la derecha
              nodo2 = nodo1->derecha;
              // se hace una igualacion de los nodos izquierda y dercha
              nodo0->izquierda = nodo2->derecha;
              nodo2->derecha = nodo0;
              // se hace una comparativa por derecha e izquierda
              nodo1->derecha = nodo2->izquierda;
              nodo2->izquierda = nodo1;
              // si el factor de equilibrio es -1
              if (nodo2->factor_equilibrio == -1)
              // el nodo 0 tendra un factor 1
                nodo0->factor_equilibrio = 1;
              else
                // el nodo 0 tendra un factor 0
                nodo0->factor_equilibrio = 0;
              // si el factor de equilibrio del nodo 2 es 1
              if (nodo2->factor_equilibrio == 1)
                // el nodo 1 tendra un factor -1
                nodo1->factor_equilibrio = -1;
              else
              // el nodo 1 tendra un factor 0
                nodo1->factor_equilibrio = 0;
              nodo0 = nodo2;
            }
            // luego el nodo 0 tendra un factor de equilibrio 0 y se termina este ciclo
            nodo0->factor_equilibrio = 0;
            *altura_arbol = false;
            break;
        }
      }

    } else {
      // si el dato es mayor a la informacion
      if (dato_id > nodo0->informacion_dato){
        // se insertara por la derecha el dato
        InsercionBalanceado(&(nodo0->derecha), altura_arbol, dato_id);
        // si la altura es verdadera
        if (*altura_arbol == true) {
          // se hara un ciclo switch case de los factores de equilibrio
          switch (nodo0->factor_equilibrio) {
            // En el caso -1 el factor es igual a 0
            case -1:
              nodo0->factor_equilibrio = 0;
              *altura_arbol = false;
              break;
              // En el caso 0 el factor es igual a 1
            case 0:
              nodo0->factor_equilibrio = 1;
              break;
              // en el caso 1
            case 1:
              // se reestructura el arbol por la derecha
              nodo1 = nodo0->derecha;
              // si el factor de equilibrio es mayor y menor a 0
              if (nodo1->factor_equilibrio >= 0) {
                // se hace una rotacion DD
                // se ve la derecha e izquierda del nodo
                nodo0->derecha = nodo1->izquierda;
                nodo1->izquierda = nodo0;
                // El factor de equilibrio sera igual a 0
                nodo0->factor_equilibrio = 0;
                nodo0 = nodo1;

              } else {
                // Se realiza una rotacion DI
                nodo2 = nodo1->izquierda;
                // se vera de la derecha a izquierda
                nodo0->derecha = nodo2->izquierda;
                nodo2->izquierda = nodo0;
                // se vera de izquierda a la derecha
                nodo1->izquierda = nodo2->derecha;
                nodo2->derecha = nodo1;
                // si el nodo2 tiene el factor de equilibrio igual a 1
                if (nodo2->factor_equilibrio == 1)
                // el nodo 0 tiene un factor de equilibrio igual a -1
                  nodo0->factor_equilibrio = -1;
                else
                  // El factor de equilibrio sera igual a 0
                  nodo0->factor_equilibrio = 0;
                // si el nodo2 tiene el factor de equilibrio igual a -1
                if (nodo2->factor_equilibrio == -1)
                // el nodo1 tendra el factor de equilibrio a 1
                  nodo1->factor_equilibrio = 1;
                else
                  // El factor de equilibrio sera igual a 0
                  nodo1->factor_equilibrio = 0;
                nodo0 = nodo2;
              }
                // El factor de equilibrio del nodo0 sera igual a 0
              nodo0->factor_equilibrio = 0;
              *altura_arbol = false;
              break;
          }
        }
      } else {
        // se imprime que el nodo esta en el arbol
        cout << " EL NODO ESTA EN EL ARBOL\n" << endl;
      }
    }
  } else {
    nodo0 = new Nodo; // se genera un nuevo nodo
    nodo0->izquierda = NULL; // el nodo 0 por la izquierda sera vacio
    nodo0->derecha = NULL; // el nodo 0 por la derecha sera vacio
    nodo0->informacion_dato = dato_id; // la informacion del dato es igualada al dato_id
    nodo0->factor_equilibrio = 0;  // el factor de equilibrio es 0
    *altura_arbol = true;
  }
  *raiz = nodo0;
}

// Esta funciuon reestructura el nodo por la derecha
void Arbol::ReestructuraDer(Nodo **raiz, int *altura_arbol){
  // se declaran los nodos sus variables y nombres
    Nodo *nodo0;
    Nodo *nodo1;
    Nodo *nodo2;
      // el nodo se iguala a la raiz
    nodo0 = *raiz;
    // si altura del arbol es igual a verdadero
    if(*altura_arbol == true){
      // SE HACEN COMPARACIONES  DEL FACTOR DE EQUILIBRIO DEL NODO 0
        switch(nodo0->factor_equilibrio){
            case -1:
                // factor de equilibrio igual a -1, el nodo 0 sera igual a 0
                nodo0->factor_equilibrio = 0;
                break;

            case 0:
                // factor de equilibrio igual a 0, el nodo 0 sera igual a 1
                nodo0->factor_equilibrio = 1;
                // la altura sera falsa
                *altura_arbol = false;
                break;

            case 1:
                // factor de equilibrio igual a 1
                // Reestructuración del árbol
                nodo1 = nodo0->derecha;
                // El nodo1 tiene un factor mayor igual a 0
                if(nodo1->factor_equilibrio >= 0){
                    // Se rota por la DD
                    nodo0->derecha = nodo1->izquierda;
                    nodo1->izquierda = nodo0;
                    // SE HACEN COMPARACIONES  DEL FACTOR DE EQUILIBRIO DEL NODO 1
                    switch(nodo1->factor_equilibrio){
                        case 0:
                        // CUANDO ES IGUAL A 0
                            nodo0->factor_equilibrio = 1; // nodo0 tiene el factor de equilibrio igual a 1
                            nodo1->factor_equilibrio = -1; // nodo1 tiene el factor de equilibrio igual a -1
                            *altura_arbol = false;    // la altura sera falsa
                            break;
                        case 1:
                        // CUANDO ES IGUAL A 1
                            nodo0->factor_equilibrio = 0;  // nodo0 tiene el factor de equilibrio igual a 0
                            nodo1->factor_equilibrio = 0;  // nodo1 tiene el factor de equilibrio igual a 0
                            *altura_arbol = false;        // la altura sera falsa
                    }
                    nodo0 = nodo1;
                }
                else{
                  // SE HACE UNA ROTACION DI
                    nodo2 = nodo1->izquierda;
                    // se recoorre de derecha a izquierda (nodo9 y 2)
                    nodo0->derecha = nodo2->izquierda;
                    nodo2->izquierda = nodo0;
                    // luego se ve el nodo de la izquierda a derecha (nodo 1 y 2)
                    nodo1->izquierda = nodo2->derecha;
                    nodo2->derecha = nodo1;

                    if(nodo2->factor_equilibrio = 1){ // el nodo2 tiene un factor igual a 1
                        nodo0->factor_equilibrio = -1;  // nodo0 tiene el factor de equilibrio igual a -1
                    }
                    else{
                        nodo0->factor_equilibrio = 0; // nodo0 tiene el factor de equilibrio igual a 0
                    }
                    if(nodo2->factor_equilibrio = -1){ // el nodo2 tiene un factor igual a -1
                        nodo1->factor_equilibrio = 1;  // nodo1 tiene el factor de equilibrio igual a 1
                    }
                    else{
                        nodo1->factor_equilibrio = 0; // nodo1 tiene el factor de equilibrio igual a 0
                    }

                    nodo0 = nodo2;
                    nodo2->factor_equilibrio = 0; // nodo2 tiene el factor de equilibrio igual a 0
                }
                break;
        }
    }
    // la raiz se iguala al nodo 0
    *raiz = nodo0;
}

// Esta funcion reestructura el nodo por la izquierda
void Arbol::ReestructuraIzq(Nodo **raiz, int *altura_arbol) {
    // se declaran los nodos sus variables y nombres
    Nodo *nodo0;
    Nodo *nodo1;
    Nodo *nodo2;
    // el nodo se iguala a la raiz
    nodo0 = *raiz;
    // si la altura es igual a verdadero
    if (*altura_arbol == true) {
      // SI EL FACTOR DE EQUILIBRIO DEL NODO 0 CUMPLE LAS CONDICIONES SIGUIENTES SUCEDE QUE:
        switch (nodo0->factor_equilibrio) {
            case 1:
            // factor de equilibrio igual a 1, el nodo 0 sera igual a 1
                nodo0->factor_equilibrio = 0;
                break;

            case 0:
            // factor de equilibrio igual a 0, el nodo 0 sera igual a -1
                nodo0->factor_equilibrio = -1;
                *altura_arbol = false;    //la altura sera falsa
                break;

            case -1:
            // factor de equilibrio igual a -1
            // Se reestructura el ARBOL
            // el nodo 1 es igual al nodo 0 por la izquierda
                nodo1 = nodo0->izquierda;
                // si el nodo 1 tiene un factor de equilibrio menor o igual a 0
                if(nodo1->factor_equilibrio <= 0){
                    //se hace una rotacion II
                    nodo0->izquierda = nodo1->derecha; // SE recoorre del nodo 0 por la izquierda, al nodo 1 por la derecha
                    nodo1->derecha = nodo0;
                      // SE HACEN COMPARACIONES  DEL FACTOR DE EQUILIBRIO DEL NODO 1
                    switch(nodo1->factor_equilibrio){
                        case 0:
                        // cuando es 0
                            nodo0->factor_equilibrio = -1; // el factor del nodo 0 es -1
                            nodo1->factor_equilibrio = 1;  //el factor del nodo 1 es 1
                            *altura_arbol = false; // la altura es falsa
                            break;

                        case -1:
                            nodo0->factor_equilibrio = 0; // el factor del nodo 0 es -0
                            nodo1->factor_equilibrio = 0; //el factor del nodo 1 es 0
                            *altura_arbol = false;  // la altura es falsa
                            break;
                    }

                    nodo0 = nodo1;
                }
                else {
                    // se hace la rotacion por ID
                    nodo2 = nodo1->derecha;
                    // del nodo 0 se recorre de la izquierda al nodo2 por la derecha
                    nodo0->izquierda = nodo2->derecha;
                    nodo2->derecha = nodo0;
                    // del nodo 1 por la derecha se recorre al nodo2 por la izquierda
                    nodo1->derecha = nodo2->izquierda;
                    nodo2->izquierda = nodo1;

                    if(nodo2->factor_equilibrio == -1){  //  si el factor del nodo 2 es -1
                        nodo0->factor_equilibrio = 1; // el factor del nodo 0 es 1
                    }
                    else{
                        nodo0->factor_equilibrio = 0; // de modo contrario el nodo 0 tendra un factor de equilibrio 0
                    }
                    if (nodo2->factor_equilibrio == 1){   //  si el factor de equilibrio del nodo 2 es 1
                        nodo1->factor_equilibrio = -1;  // el factor del nodo 1 es -1
                    }
                    else{
                        nodo1->factor_equilibrio = 0; // de modo contrario el nodo 1 tendra un factor de equilibrio 0
                    }

                    nodo0 = nodo2;
                    nodo2->factor_equilibrio = 0; // cuando se iguala el nodo0 y 2 el factor del nodo 2 sera igual a 0
                }
                break;
        }
    }
    // la raiz se iguala al nodo0
    *raiz = nodo0;
}

// Se define la funcion que borra el nodo
void Arbol::Borra(Nodo **aux1, Nodo **otro1, int *altura_arbol) {
  // Se definen los nodos a implementar y tambien el tipo de las variables
  Nodo *aux;
  Nodo *otro;
  // se definen auxiliares de busqueda
  aux = *aux1;
  otro = *otro1;
  // si el auxiliar por la derecha es diferente de null
  if (aux->derecha != NULL) {
    Borra(&(aux->derecha),&otro,altura_arbol);  // lo borra el nodo por la derecha
    ReestructuraIzq(&aux, altura_arbol);         // lo reestructura por la izquierda
  } else {
    otro->informacion_dato = aux->informacion_dato; // si los datos de auxiliar y otros son iguales
    aux = aux->izquierda; // auxiliar recorre la izquierda
    *altura_arbol = true;
  }
  *aux1=aux;
  *otro1=otro;
}

// Esta funcion se encarga de eliminar balanceadamente los datos
void Arbol::EliminarBalanceado(Nodo **raiz, int *altura_arbol, string dato_id) {
  // Se definen los nodos de ayuda y otros auxiliares con su tipo
  Nodo *otro_auxiliar;
  Nodo *nodo0;
  nodo0 = *raiz;
  // si el nodo es distinto de vacio
  if (nodo0 != NULL) {
    // se compara el dato si es menor a la informacion
    if (dato_id < nodo0->informacion_dato) {
      EliminarBalanceado(&(nodo0->izquierda),altura_arbol,dato_id); // Se llama a la funcion eliminar por la izquierda
      ReestructuraDer(&nodo0, altura_arbol);  // se llama a la funcion que reestructura por la derecha
    } else {
      // si el dato es mayor a la informacion
      if (dato_id > nodo0->informacion_dato) {
        EliminarBalanceado(&(nodo0->derecha),altura_arbol,dato_id);  // Se llama a la funcion eliminar por la derecha
        ReestructuraIzq(&nodo0, altura_arbol);  // se llama a la funcion que reestructura por la izquierda
      } else {
        // se define otro auxiliar igual al nodo0
        otro_auxiliar = nodo0;
        // si el auxiliar por la derecha es vacia
        if (otro_auxiliar->derecha == NULL) {
          nodo0 = otro_auxiliar->izquierda; //Nodo y otro_auxiliar va hacia la izquierda
          *altura_arbol = true; // la altura del arbol es verdadera
        } else {
            // si el auxiliar por la derecha es vacia
          if (otro_auxiliar->izquierda==NULL) {
            nodo0=otro_auxiliar->derecha; //Nodo y otro_auxiliar va hacia la derecha
            *altura_arbol=true;
          } else {
            // de modo contrario se llama a la funcion que borra el nodo y restructrua a la derecha
            Borra(&(otro_auxiliar->izquierda), &otro_auxiliar, altura_arbol);
            ReestructuraDer(&nodo0,altura_arbol);
            free(otro_auxiliar); // el dato auxiliar es liberado
          }
        }
      }
    }
  } else {
    cout <<"El nodo NO se encuentra en el árbol" << endl;
  }
  *raiz=nodo0;
}

// Esta funcion retornara el puntero a un valor ingresado
bool Arbol::busqueda_nodo(Nodo *nodo0, string dato_id) {
  if (nodo0 != NULL) {
    // si el puntero es diferente de vacio se recorre el nodo con la informacion cuando es mayor al dato
    if (dato_id < nodo0->informacion_dato) {
      // se busca por la izquierda el nodo y el dato
      busqueda_nodo(nodo0->izquierda, dato_id);
    }
    else {
      // se recorre cuando el dato es menor a la informacion
      if (dato_id > nodo0->informacion_dato) {
        // se busca por la derecha el nodo y el dato
        busqueda_nodo(nodo0->derecha, dato_id);
      }
      else{
        // Se retorna un true
        return true;
      }
    }
  }
  // Si el nodo esta vacio se retorna un False que no esta en el nodo
  if (nodo0 == NULL){
    return false;
  }
}

// Esta funcion de tipo booleana permite modificar el nodo, y verificar que esten los numeros a modificar
bool Arbol::modificacion_nodo(Nodo **raiz, int *altura_arbol, string dato_id, string dato_nuevo){
 // Se definen las variables de la funcion y tambien el tipo de ellas.
 bool resultado;
 bool resultado2;
 bool operacion;
 operacion = false;
 // se establece una comparativa entre la funcion de busqueda y resultado
 resultado = this->busqueda_nodo(*raiz, dato_id);
 if(resultado == false){					// Si la busqueda del dato es falsa las operaciones no se pueden completar
 cout << endl << "No se puede completar la operacion el Dato no existe" << endl; // SE IMPRIME QUE EL DATO NO EXISTE
 }else{
   // Se establece una nueva comparativa en el caso de que el resultado2 sea verdadera
   resultado2 = this->busqueda_nodo(*raiz, dato_nuevo);
   if(resultado2 == true){
     cout << endl << "No se puede completar la operacion el Dato a Modificar existe" << endl; //SE IMPRIME QUE EL DATO NO PUEDE SER MODIFICADO
   }else{
     // de modo contrario se modifica el dato
     cout<<"Se va a modificar el dato"<<endl;
     // Se llaman a las funciones
     EliminarBalanceado(raiz,altura_arbol,dato_id); // se elimina antiguo dato
     InsercionBalanceado(raiz, altura_arbol, dato_nuevo); // se agrega el nuevo dato
     operacion = true; // la operacion sera verdadera
   }
 }
 return operacion;
}

  // FUNCION QUE LEE LAS TODOS LOS ID DE LAS PROTEINAS
void Arbol::leer_datos(Nodo **raiz, int *altura_arbol){
  string id_proteinas;
  // se lee el archivo
  ifstream read("pdblist.txt");
  // Se recorre un ciclo para captar los datos
  while (!read.eof()) {
    // se le otorga la variable de las id_proteinas a los datos del archivo
    read >> id_proteinas;
    // Se insertan en el arbol los id
    this-> InsercionBalanceado(raiz, altura_arbol, id_proteinas);
  }
  // la lectura se termina
  read.close();
}

// FUNCION QUE LEE LA LISTA DE DATOS DE ID MAS PEQUEÑA
void Arbol::leer_datos2(Nodo **raiz, int *altura_arbol){
  string id_proteinas;
  // se lee el archivo
  ifstream read("pdblist2.txt");
  // Se recorre un ciclo para captar los datos
  while (!read.eof()) {
      // se le otorga la variable de las id_proteinas a los datos del archivo
    read >> id_proteinas;
    // Se insertan en el arbol los id
    this-> InsercionBalanceado(raiz, altura_arbol, id_proteinas);
  }
  // la lectura se termina
  read.close();
}
